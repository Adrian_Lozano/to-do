package com.softtek.academy.javaweb.exam.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.exam.beans.ToDoBean;
import com.softtek.academy.javaweb.exam.dao.ToDoDao;

/**
 * Servlet implementation class ControllerServletListToDo
 */

public class ControllerServletListToDo extends HttpServlet {
	private static final long serialVersionUID = 1092735L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServletListToDo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List <ToDoBean> list = ToDoDao.getAllUnDoneTask();
		request.getSession().setAttribute("listToDo", list);
		response.sendRedirect(request.getContextPath() + "/views/ToDoShowUnDone.jsp");
	}

}
