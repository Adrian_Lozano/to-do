package com.softtek.academy.javaweb.exam.dao;

import java.util.List;
import java.util.stream.Collectors;
import com.softtek.academy.javaweb.exam.beans.ToDoBean;
import com.softtek.academy.javaweb.exam.service.ToDoService;

public class ToDoDao {
	
	public static void insertNew(String newTask) {
		if(newTask != null) {
			ToDoService.insertNewTask(newTask);
		}
	}
	
	public static List<ToDoBean> getAllUnDoneTask(){
		List<ToDoBean> unDoneTask = ToDoService.getAll().stream()
				.filter(d -> d.getIsDone() == false)
				.collect(Collectors.toList());
		return unDoneTask;
	}
	
	public static List<ToDoBean> getAllDoneTask(){
		List<ToDoBean> DoneTask = ToDoService.getAll().stream()
				.filter(d -> d.getIsDone() == true)
				.collect(Collectors.toList());
		return DoneTask;
	}
	public static List<ToDoBean> updateRecord(int id){
		ToDoService.updateRecord(id);
		List<ToDoBean> updateList = getAllDoneTask();
		return updateList;
	}
}
