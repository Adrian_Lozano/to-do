package com.softtek.academy.javaweb.exam.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.exam.beans.ToDoBean;

public class ToDoService {
	public ToDoService() {
		super();
	}
	public static void insertNewTask(String newTask) {
		DBConnection conn = new DBConnection();
		String SQL_INSERT = "INSERT INTO to_do_list (list) values (?)";
		
		PreparedStatement preparedStatement;
		try {
			preparedStatement = conn.getConnection().prepareStatement(SQL_INSERT);
			preparedStatement.setString(1, newTask);
			preparedStatement.execute();
			
		} catch(SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	public static List<ToDoBean> getAll(){
		List<ToDoBean> result = new ArrayList<>();
		DBConnection conn = new DBConnection();
		String SQL_SELECT = "SELECT * FROM to_do_list";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = conn.getConnection().prepareStatement(SQL_SELECT);

			ResultSet resultSet = preparedStatement.executeQuery();
			

			while (resultSet.next()) {
				ToDoBean ToDo = new ToDoBean();
				ToDo.setId(resultSet.getInt("id"));
				ToDo.setList(resultSet.getString("list"));
				ToDo.setIsDone(resultSet.getBoolean("is_done"));
				result.add(ToDo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public static void updateRecord(int id) {
		DBConnection conn = new DBConnection();
		String SQL_UPDATE = "UPDATE to_do_list set IS_DONE = 1 where id = ?";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = conn.getConnection().prepareStatement(SQL_UPDATE);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
			}
	}
}
