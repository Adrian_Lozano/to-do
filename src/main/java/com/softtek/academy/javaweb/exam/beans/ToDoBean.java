package com.softtek.academy.javaweb.exam.beans;

public class ToDoBean {
	 private int id;
	 private String list;
	 private boolean isDone;
	
	public ToDoBean() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getList() {
		return list;
	}
	public void setList(String list) {
		this.list = list;
	}
	public boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(boolean isDone) {
		this.isDone = isDone;
	}
}
