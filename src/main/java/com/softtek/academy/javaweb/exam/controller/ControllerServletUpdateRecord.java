package com.softtek.academy.javaweb.exam.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.exam.beans.ToDoBean;
import com.softtek.academy.javaweb.exam.dao.ToDoDao;

/**
 * Servlet implementation class ControllerServletUpdateRecord
 */
//@WebServlet("/ControllerServletUpdateRecord")
public class ControllerServletUpdateRecord extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServletUpdateRecord() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idRecord  = Integer.parseInt(request.getParameter("idRecord"));
		List <ToDoBean> listUpdated = ToDoDao.updateRecord(idRecord);
		request.getSession().setAttribute("listDone", listUpdated);
		response.sendRedirect(request.getContextPath() + "/views/DoneShow.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
