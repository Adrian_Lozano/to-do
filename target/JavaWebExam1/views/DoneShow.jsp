<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import = "java.util.*" %>
<% session.getAttribute("listDone"); 
String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Activities Marked as Done</title>
Activities Marked as Done
</head>
<body>
	<table>
		<tr>
		<td>ID</td>
		<td>Task</td>
		<td>Status</td>
		</tr>
	<c:forEach items = '${listDone}' var = "listDone">

		<tr>	
		<td> <c:out value="${listDone.getId()}"></c:out>  </td>
		<td> <c:out value="${listDone.getList()}"></c:out></td>
		<td> Done!</td>	
	</tr>
	</c:forEach>
</table>
<a href="<%= contextPath %>">Main page</a>
</body>
</html>